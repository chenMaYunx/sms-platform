package com.qianfeng.smsplatform.common.constants;

public class CacheConstants {
    public final static String CACHE_PREFIX_CLIENT = "six_CLIENT:";
    public final static String CACHE_PREFIX_PHASE = "six_PHASE:";
    public final static String CACHE_PREFIX_BLACK = "six_BLACK:";
    public final static String CACHE_PREFIX_CUSTOMER_FEE = "six_CUSTOMER_FEE:";
    public final static String CACHE_PREFIX_ROUTER = "six_ROUTER:";
    public final static String CACHE_PREFIX_DIRTYWORDS = "six_DIRTYWORDS:";

    public final static String CACHE_PREFIX_SMS_LIMIT_FIVE_MINUTE = "six_LIMIT_5_MINUTE:";
    public final static String CACHE_PREFIX_SMS_LIMIT_HOUR = "six_LIMIT_HOUR:";
    public final static String CACHE_PREFIX_SMS_LIMIT_DAY = "six_LIMIT_DAY:";

}
